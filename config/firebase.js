const admin = require("firebase-admin");

const serviceAccount = require("./myproject-98efa-firebase-adminsdk-9t0rz-4efa09aa6c.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

const db = admin.firestore();

module.exports = db;
