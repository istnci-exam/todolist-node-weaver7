const router = require("express").Router();
const TodolistService = require("../services/Todolist.service");

router.get("/", async (req, res) => {
  try {
    const user_id = req.headers.user_id;
    const result = await TodolistService.getTodolist(user_id);

    res.status(200).send(result);
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.post("/", async (req, res) => {
  try {
    const todolist = req.body;
    const result = await TodolistService.updateTodolist(todolist);

    res.status(200).send(result);
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.post("/message", async (req, res) => {
  try {
    const user_id = req.body.user_id;
    const resultConversation = await TodolistService.openConversation(user_id);

    const conversation_id = resultConversation.data.conversation.id;
    const resultMessage = await TodolistService.sendMessage(conversation_id);

    res.status(200).send(resultMessage.data);
  } catch (error) {
    res.status(500).send(error.message);
  }
});

module.exports = router;
